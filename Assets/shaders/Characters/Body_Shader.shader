// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "DDD/Body_Shader"
{
	Properties
	{
		_ASEOutlineWidth( "Outline Width", Float ) = 0.05
		_ASEOutlineColor( "Outline Color", Color ) = (0.1470588,0.1470588,0.1470588,0)
		_Difuse("Difuse", 2D) = "white" {}
		_BrilloDifuse("Brillo Difuse", Range( 1 , 2)) = 1
		_PowDiffuse("Pow Diffuse", Range( 1 , 2)) = 1
		_Degradados("Degradados", 2D) = "white" {}
		_ShadowContrast("ShadowContrast", Range( 1 , 1.5)) = 1
		_ShadowColor("Shadow Color", Color) = (1,0.5588235,0.5588235,0)
		_ShadowPower("Shadow Power", Range( 0.01 , 0.3)) = 0.18
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ }
		Cull Front
		CGPROGRAM
		#pragma target 3.0
		#pragma surface outlineSurf Outline nofog keepalpha noshadow noambient novertexlights nolightmap nodynlightmap nodirlightmap nometa noforwardadd vertex:outlineVertexDataFunc 
		uniform fixed4 _ASEOutlineColor;
		uniform fixed _ASEOutlineWidth;
		void outlineVertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			v.vertex.xyz += ( v.normal * _ASEOutlineWidth );
		}
		inline fixed4 LightingOutline( SurfaceOutput s, half3 lightDir, half atten ) { return fixed4 ( 0,0,0, s.Alpha); }
		void outlineSurf( Input i, inout SurfaceOutput o )
		{
			o.Emission = _ASEOutlineColor.rgb;
			o.Alpha = 1;
		}
		ENDCG
		

		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldNormal;
			INTERNAL_DATA
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			fixed3 Albedo;
			fixed3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			fixed Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float _BrilloDifuse;
		uniform sampler2D _Difuse;
		uniform float4 _Difuse_ST;
		uniform float _PowDiffuse;
		uniform float _ShadowContrast;
		uniform sampler2D _Degradados;
		uniform float4 _ShadowColor;
		uniform float _ShadowPower;


		float4 CalculateContrast( float contrastValue, float4 colorTarget )
		{
			float t = 0.5 * ( 1.0 - contrastValue );
			return mul( float4x4( contrastValue,0,0,t, 0,contrastValue,0,t, 0,0,contrastValue,t, 0,0,0,1 ), colorTarget );
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#if DIRECTIONAL
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			float2 uv_Difuse = i.uv_texcoord * _Difuse_ST.xy + _Difuse_ST.zw;
			float4 temp_cast_0 = (_PowDiffuse).xxxx;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			float dotResult108 = dot( ase_worldNormal , ase_worldlightDir );
			float temp_output_113_0 = saturate( (dotResult108*0.49 + 0.49) );
			float2 temp_cast_1 = (temp_output_113_0).xx;
			float4 tex2DNode120 = tex2D( _Degradados, temp_cast_1 );
			float4 temp_output_242_0 = CalculateContrast(_ShadowContrast,tex2DNode120);
			float4 lerpResult197 = lerp( temp_output_242_0 , _ShadowColor , ( 1.0 - temp_output_242_0 ));
			UnityGI gi247 = gi;
			float3 diffNorm247 = ase_worldNormal;
			gi247 = UnityGI_Base( data, 1, diffNorm247 );
			float3 indirectDiffuse247 = gi247.indirect.diffuse + diffNorm247 * 0.0001;
			float4 temp_cast_3 = (_ShadowPower).xxxx;
			c.rgb = ( CalculateContrast(_BrilloDifuse,pow( tex2D( _Difuse, uv_Difuse ) , temp_cast_0 )) * pow( ( lerpResult197 * ( _LightColor0 * float4( ( indirectDiffuse247 + ase_lightAtten ) , 0.0 ) ) ) , temp_cast_3 ) ).rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14201
2083;70;1668;919;2970.793;1442.67;1.856122;True;False
Node;AmplifyShaderEditor.CommentaryNode;100;-4141.669,-741.1825;Float;False;558.214;366.9138;Comment;3;103;101;108;N . L;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;103;-4074.105,-529.6194;Float;False;1;0;FLOAT;0.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldNormalVector;101;-4033.231,-689.6195;Float;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CommentaryNode;102;-3556.989,-736.1655;Float;False;723.599;290;Lambert Wrap or Half Lambert;3;113;111;107;Diffuse Wrap;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;107;-3541.59,-523.6758;Float;False;Constant;_DivisionStrenght;Division Strenght;3;0;Create;0.49;0.45;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;108;-3741.663,-629.1825;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0.0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;111;-3258.885,-633.5656;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;1.0;False;2;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;113;-3016.03,-673.4247;Float;True;1;0;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;120;-2712.238,-709.3025;Float;True;Property;_Degradados;Degradados;4;0;Create;2429be309ec037b4c99f6b10bbee81aa;194f3ca1259a1d241a675089bdcc60b1;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;241;-2393.475,-516.4493;Float;False;Property;_ShadowContrast;ShadowContrast;5;0;Create;1;1.5;1;1.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;242;-2185.836,-707.0274;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;247;-1422.353,131.2306;Float;False;Tangent;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightAttenuation;246;-1427.563,265.1317;Float;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;188;-1786.161,-732.2921;Float;False;Property;_ShadowColor;Shadow Color;6;0;Create;1,0.5588235,0.5588235,0;1,0.6862745,0.6862745,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;198;-1777.371,-485.6569;Float;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;243;-1777.318,-791.443;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;248;-1148.052,131.2314;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0.0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightColorNode;211;-1331.034,-39.42378;Float;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;186;-1168.099,-1075.758;Float;False;Property;_PowDiffuse;Pow Diffuse;2;0;Create;1;1;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;193;-698.9698,-892.5579;Float;False;539.3658;311.6136;Obscurece las sombras;2;189;192;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;249;-981.4473,-39.16908;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;119;-1185.9,-1281.183;Float;True;Property;_Difuse;Difuse;0;0;Create;84508b93f15f2b64386ec07486afc7a3;92f6a5893ac768c41b25716b1575587e;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;197;-1256.207,-767.8835;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;2;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;205;-825.8192,-992.3426;Float;False;Property;_BrilloDifuse;Brillo Difuse;1;0;Create;1;1.477;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;185;-797.1777,-1225.144;Float;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;192;-674.6374,-767.7398;Float;False;Property;_ShadowPower;Shadow Power;7;0;Create;0.18;0.169;0.01;0.3;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;213;-850.1127,-392.9962;Float;False;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;204;-441.8385,-1161.993;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;189;-361.4044,-788.7595;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;244;-2407.935,-831.8324;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;123;15.38579,-819.4236;Float;False;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;218;-2731.494,-1004.632;Float;True;Property;_Degradados2;Degradados 2;3;0;Create;2429be309ec037b4c99f6b10bbee81aa;64b282229066c7444aff8dadc9fe8866;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;250;-1494.416,-1113.468;Float;True;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;588.8524,-821.7889;Float;False;True;2;Float;ASEMaterialInspector;0;0;CustomLighting;DDD/Body_Shader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;True;0.05;0.1470588,0.1470588,0.1470588,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0.0,0,0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;108;0;101;0
WireConnection;108;1;103;0
WireConnection;111;0;108;0
WireConnection;111;1;107;0
WireConnection;111;2;107;0
WireConnection;113;0;111;0
WireConnection;120;1;113;0
WireConnection;242;1;120;0
WireConnection;242;0;241;0
WireConnection;198;0;242;0
WireConnection;243;0;242;0
WireConnection;248;0;247;0
WireConnection;248;1;246;0
WireConnection;249;0;211;0
WireConnection;249;1;248;0
WireConnection;197;0;243;0
WireConnection;197;1;188;0
WireConnection;197;2;198;0
WireConnection;185;0;119;0
WireConnection;185;1;186;0
WireConnection;213;0;197;0
WireConnection;213;1;249;0
WireConnection;204;1;185;0
WireConnection;204;0;205;0
WireConnection;189;0;213;0
WireConnection;189;1;192;0
WireConnection;244;0;218;0
WireConnection;244;1;120;0
WireConnection;123;0;204;0
WireConnection;123;1;189;0
WireConnection;218;1;113;0
WireConnection;250;0;242;0
WireConnection;250;1;188;0
WireConnection;0;13;123;0
ASEEND*/
//CHKSM=419FFD64D04093FFADC60AE3CAA4CD94B192D9BF