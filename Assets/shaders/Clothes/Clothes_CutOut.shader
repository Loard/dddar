// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "DDD/Clothes_CutOut"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_StyleMask("Style Mask", 2D) = "white" {}
		[Toggle]_SetPatron("Set/Patron", Float) = 0
		_Set("Set", 2D) = "white" {}
		_Patron("Patron", 2D) = "white" {}
		_PatternColor("Pattern Color", Color) = (0,0.8344827,1,0)
		_ClothesColor("Clothes Color", Color) = (1,0,0,0)
		_BrilloDifuse("Brillo Difuse", Range( 1 , 2)) = 1
		_PowDiffuse("Pow Diffuse", Range( 1 , 2)) = 1
		_Degradado("Degradado", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" }
		Cull Off
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldNormal;
			INTERNAL_DATA
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			fixed3 Albedo;
			fixed3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			fixed Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _StyleMask;
		uniform float4 _StyleMask_ST;
		uniform float _BrilloDifuse;
		uniform float _SetPatron;
		uniform sampler2D _Set;
		uniform float4 _Set_ST;
		uniform sampler2D _Patron;
		uniform float4 _Patron_ST;
		uniform float4 _PatternColor;
		uniform float4 _ClothesColor;
		uniform float _PowDiffuse;
		uniform sampler2D _Degradado;
		uniform float _Cutoff = 0.5;


		float4 CalculateContrast( float contrastValue, float4 colorTarget )
		{
			float t = 0.5 * ( 1.0 - contrastValue );
			return mul( float4x4( contrastValue,0,0,t, 0,contrastValue,0,t, 0,0,contrastValue,t, 0,0,0,1 ), colorTarget );
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#if DIRECTIONAL
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			float2 uv_StyleMask = i.uv_texcoord * _StyleMask_ST.xy + _StyleMask_ST.zw;
			float2 uv_Set = i.uv_texcoord * _Set_ST.xy + _Set_ST.zw;
			float2 uv_Patron = i.uv_texcoord * _Patron_ST.xy + _Patron_ST.zw;
			float4 temp_output_260_0 = pow( tex2D( _Patron, uv_Patron ) , 2.0 );
			float4 lerpResult255 = lerp( ( ( 1.0 - temp_output_260_0 ) * _PatternColor ) , _ClothesColor , temp_output_260_0);
			float4 temp_cast_0 = (_PowDiffuse).xxxx;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			float dotResult108 = dot( ase_worldNormal , ase_worldlightDir );
			float2 temp_cast_1 = (saturate( (dotResult108*0.49 + 0.49) )).xx;
			UnityGI gi261 = gi;
			float3 diffNorm261 = ase_worldNormal;
			gi261 = UnityGI_Base( data, 1, diffNorm261 );
			float3 indirectDiffuse261 = gi261.indirect.diffuse + diffNorm261 * 0.0001;
			float4 temp_cast_3 = (0.01).xxxx;
			c.rgb = ( CalculateContrast(_BrilloDifuse,pow( lerp(tex2D( _Set, uv_Set ),lerpResult255,_SetPatron) , temp_cast_0 )) * CalculateContrast(1.1,pow( ( CalculateContrast(1.2,tex2D( _Degradado, temp_cast_1 )) * ( _LightColor0 * float4( ( indirectDiffuse261 + ase_lightAtten ) , 0.0 ) ) ) , temp_cast_3 )) ).rgb;
			c.a = 1;
			clip( tex2D( _StyleMask, uv_StyleMask ).a - _Cutoff );
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				UnityGI gi;
				UNITY_INITIALIZE_OUTPUT( UnityGI, gi );
				LightingStandardCustomLighting( o, worldViewDir, gi );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14201
1927;29;1906;1014;5568.762;2518.49;3.836458;True;False
Node;AmplifyShaderEditor.CommentaryNode;100;-5075.068,-345.5008;Float;False;558.214;366.9138;Comment;3;103;101;108;N . L;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldNormalVector;101;-4966.631,-293.9378;Float;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CommentaryNode;167;-3718.344,-2010.865;Float;False;3264.547;1568.312;Cambiamos el color y el patron;23;280;279;281;276;119;275;277;204;205;185;251;186;255;145;132;245;267;254;135;260;146;284;285;Color and Pattern Change;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;102;-4490.397,-340.4839;Float;False;723.599;290;Lambert Wrap or Half Lambert;3;113;111;107;Diffuse Wrap;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;103;-5007.506,-133.9379;Float;False;1;0;FLOAT;0.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;146;-3596.551,-1797.958;Float;True;Property;_Patron;Patron;4;0;Create;84508b93f15f2b64386ec07486afc7a3;cd460ee4ac5c1e746b7a734cc7cc64dd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;107;-4444.872,-183.6148;Float;True;Constant;_DivisionStrenght;Division Strenght;3;0;Create;0.49;0.49;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;108;-4675.068,-233.5009;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0.0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;260;-3243.427,-1791.34;Float;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;2.0;False;1;COLOR;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;111;-4178.392,-288.8695;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;1.0;False;2;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;254;-2748.254,-1791.491;Float;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;135;-2789.723,-1560.965;Float;False;Property;_PatternColor;Pattern Color;5;0;Create;0,0.8344827,1,0;1,0.8516228,0.2793103,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.IndirectDiffuseLighting;261;-2911.709,537.6899;Float;False;Tangent;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightAttenuation;262;-2916.919,671.5908;Float;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;113;-3940.178,-261.5202;Float;True;1;0;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;267;-2722.064,-1155.211;Float;False;1;0;COLOR;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;245;-2405.795,-1790.862;Float;True;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;132;-2773.379,-1361.05;Float;False;Property;_ClothesColor;Clothes Color;6;0;Create;1,0,0,0;1,0.3529412,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;241;-3457.667,-65.67552;Float;False;Constant;_ShadowContrast;ShadowContrast;10;0;Create;1.2;1.2;1;1.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;263;-2820.39,367.0357;Float;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleAddOpNode;264;-2637.407,537.6907;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0.0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;218;-3569.022,-415.7745;Float;True;Property;_Degradado;Degradado;9;0;Create;2429be309ec037b4c99f6b10bbee81aa;64b282229066c7444aff8dadc9fe8866;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;193;-1879.016,-57.608;Float;False;539.3658;311.6136;Obscurece las sombras;2;189;192;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;145;-2134.235,-1961.585;Float;True;Property;_Set;Set;3;0;Create;84508b93f15f2b64386ec07486afc7a3;4ac2943049708e8439111bec285b951c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleContrastOpNode;242;-2448.726,-408.6627;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;265;-2470.804,367.2904;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;255;-1998.148,-1647.458;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;192;-1839.115,34.72403;Float;False;Constant;_ShadowPower;Shadow Power;12;0;Create;0.01;0.3;0;0.01;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;213;-2016.807,-381.5128;Float;False;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;251;-1630.719,-1959.941;Float;False;Property;_SetPatron;Set/Patron;2;0;Create;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;186;-1640.395,-1764.346;Float;False;Property;_PowDiffuse;Pow Diffuse;8;0;Create;1;1;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;194;-1160.995,108.4351;Float;False;586.2532;183;Aclara los colores;2;190;191;;1,1,1,1;0;0
Node;AmplifyShaderEditor.PowerNode;185;-1323.628,-1914.097;Float;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;205;-1347.969,-1654.941;Float;False;Property;_BrilloDifuse;Brillo Difuse;7;0;Create;1;1;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;190;-1096.125,190.0069;Float;False;Constant;_BrilloDegradado;Brillo Degradado;10;0;Create;1.1;1.1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;189;-1541.45,46.19043;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;191;-760.7368,158.4353;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;119;-820.6508,-675.0525;Float;True;Property;_StyleMask;Style Mask;1;0;Create;84508b93f15f2b64386ec07486afc7a3;4ac2943049708e8439111bec285b951c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleContrastOpNode;204;-966.468,-1859.809;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;285;-2181.652,-731.0375;Float;False;Constant;_Color2;Color 2;13;0;Create;0,0,1,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;284;-2575.248,-796.3375;Float;False;Constant;_Color1;Color 1;13;0;Create;0,1,0,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;277;-2225.416,-1041.398;Float;False;Constant;_Float1;Float 1;13;0;Create;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;275;-1874.416,-1121.398;Float;False;False;5;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;276;-2224.416,-1116.398;Float;False;Constant;_Float0;Float 0;13;0;Create;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;123;-189.6951,-575.1054;Float;False;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;286;-147.857,-640.2687;Float;False;1;0;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;279;-2203.396,-948.7254;Float;True;Property;_Gstring;Gstring;11;0;Create;0;2;0;COLOR;0.0;False;1;COLOR;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;281;-2581.079,-959.4248;Float;False;Constant;_Color0;Color 0;13;0;Create;1,0,0,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ToggleSwitchNode;280;-1921.699,-948.4254;Float;True;Property;_Short;Short;10;0;Create;0;2;0;COLOR;0.0;False;1;COLOR;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;81.01106,-807.4158;Float;False;True;2;Float;ASEMaterialInspector;0;0;CustomLighting;DDD/Clothes_CutOut;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;0;False;0;0;Masked;0.5;True;True;0;False;TransparentCutout;AlphaTest;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0.1470588,0.1470588,0.1470588,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;0;0;False;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0.0,0,0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;108;0;101;0
WireConnection;108;1;103;0
WireConnection;260;0;146;0
WireConnection;111;0;108;0
WireConnection;111;1;107;0
WireConnection;111;2;107;0
WireConnection;254;0;260;0
WireConnection;113;0;111;0
WireConnection;267;0;260;0
WireConnection;245;0;254;0
WireConnection;245;1;135;0
WireConnection;264;0;261;0
WireConnection;264;1;262;0
WireConnection;218;1;113;0
WireConnection;242;1;218;0
WireConnection;242;0;241;0
WireConnection;265;0;263;0
WireConnection;265;1;264;0
WireConnection;255;0;245;0
WireConnection;255;1;132;0
WireConnection;255;2;267;0
WireConnection;213;0;242;0
WireConnection;213;1;265;0
WireConnection;251;0;145;0
WireConnection;251;1;255;0
WireConnection;185;0;251;0
WireConnection;185;1;186;0
WireConnection;189;0;213;0
WireConnection;189;1;192;0
WireConnection;191;1;189;0
WireConnection;191;0;190;0
WireConnection;204;1;185;0
WireConnection;204;0;205;0
WireConnection;275;0;276;0
WireConnection;275;1;277;0
WireConnection;123;0;204;0
WireConnection;123;1;191;0
WireConnection;286;0;119;4
WireConnection;279;0;281;0
WireConnection;279;1;284;0
WireConnection;280;0;279;0
WireConnection;280;1;285;0
WireConnection;0;10;286;0
WireConnection;0;13;123;0
ASEEND*/
//CHKSM=B56EDE203E857570E1EFCBBFB5B0792C01794746