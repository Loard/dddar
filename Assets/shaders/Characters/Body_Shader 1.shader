// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "DDD/Body_Shader 1"
{
	Properties
	{
		_ASEOutlineWidth( "Outline Width", Float ) = 0.05
		_ASEOutlineColor( "Outline Color", Color ) = (0.1470588,0.1470588,0.1470588,0)
		_Difuse("Difuse", 2D) = "white" {}
		_BrilloDifuse("Brillo Difuse", Range( 1 , 2)) = 1
		_PowDiffuse("Pow Diffuse", Range( 1 , 2)) = 1
		_ShadowColor("Shadow Color", Color) = (1,0.5588235,0.5588235,0)
		_Degradados("Degradados", 2D) = "white" {}
		_Poder_Sombra("Poder_Sombra", Range( 0 , 0.5)) = 0.5
		_Contraste_Sombra("Contraste_Sombra", Range( 1 , 1.1)) = 1
		_Sweat_test("Sweat_test", 2D) = "white" {}
		_Sweat("Sweat", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ }
		Cull Front
		CGPROGRAM
		#pragma target 3.0
		#pragma surface outlineSurf Outline nofog keepalpha noshadow noambient novertexlights nolightmap nodynlightmap nodirlightmap nometa noforwardadd vertex:outlineVertexDataFunc 
		uniform fixed4 _ASEOutlineColor;
		uniform fixed _ASEOutlineWidth;
		void outlineVertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			v.vertex.xyz += ( v.normal * _ASEOutlineWidth );
		}
		inline fixed4 LightingOutline( SurfaceOutput s, half3 lightDir, half atten ) { return fixed4 ( 0,0,0, s.Alpha); }
		void outlineSurf( Input i, inout SurfaceOutput o )
		{
			o.Emission = _ASEOutlineColor.rgb;
			o.Alpha = 1;
		}
		ENDCG
		

		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldNormal;
			INTERNAL_DATA
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			fixed3 Albedo;
			fixed3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			fixed Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float _BrilloDifuse;
		uniform sampler2D _Difuse;
		uniform float4 _Difuse_ST;
		uniform float _PowDiffuse;
		uniform sampler2D _Sweat_test;
		uniform float4 _Sweat_test_ST;
		uniform float _Sweat;
		uniform sampler2D _Degradados;
		uniform float4 _ShadowColor;
		uniform float _Contraste_Sombra;
		uniform float _Poder_Sombra;


		float4 CalculateContrast( float contrastValue, float4 colorTarget )
		{
			float t = 0.5 * ( 1.0 - contrastValue );
			return mul( float4x4( contrastValue,0,0,t, 0,contrastValue,0,t, 0,0,contrastValue,t, 0,0,0,1 ), colorTarget );
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#if DIRECTIONAL
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			float2 uv_Difuse = i.uv_texcoord * _Difuse_ST.xy + _Difuse_ST.zw;
			float4 temp_cast_0 = (_PowDiffuse).xxxx;
			float2 uv_Sweat_test = i.uv_texcoord * _Sweat_test_ST.xy + _Sweat_test_ST.zw;
			float4 tex2DNode264 = tex2D( _Sweat_test, uv_Sweat_test );
			float4 lerpResult263 = lerp( CalculateContrast(_BrilloDifuse,pow( tex2D( _Difuse, uv_Difuse ) , temp_cast_0 )) , tex2DNode264 , ( tex2DNode264.a * _Sweat ));
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			UnityGI gi247 = gi;
			float3 diffNorm247 = ase_worldNormal;
			gi247 = UnityGI_Base( data, 1, diffNorm247 );
			float3 indirectDiffuse247 = gi247.indirect.diffuse + diffNorm247 * 0.0001;
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			float dotResult108 = dot( ase_worldNormal , ase_worldlightDir );
			float2 temp_cast_2 = (saturate( (dotResult108*0.49 + 0.49) )).xx;
			float4 tex2DNode120 = tex2D( _Degradados, temp_cast_2 );
			float4 clampResult258 = clamp( CalculateContrast(_Contraste_Sombra,( 1.0 - tex2DNode120 )) , float4( 0,0,0,0 ) , float4( 0.5882353,0.5882353,0.5882353,0 ) );
			float4 lerpResult197 = lerp( tex2DNode120 , _ShadowColor , clampResult258.r);
			float4 lerpResult254 = lerp( lerpResult263 , ( lerpResult263 * ( ( _LightColor0 * float4( ( indirectDiffuse247 + ase_lightAtten ) , 0.0 ) ) * lerpResult197 ) ) , _Poder_Sombra);
			c.rgb = lerpResult254.rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14201
2132;62;1668;919;6238.959;3584.35;1.682472;True;False
Node;AmplifyShaderEditor.CommentaryNode;252;-7069.123,-1721.925;Float;False;1854.811;555.8474;Sombras;3;120;102;100;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;100;-7027.827,-1576.218;Float;False;558.214;366.9138;Comment;3;103;101;108;N . L;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;102;-6353.96,-1558.15;Float;False;723.599;290;Lambert Wrap or Half Lambert;3;113;111;107;Diffuse Wrap;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;103;-6960.263,-1364.655;Float;False;1;0;FLOAT;0.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldNormalVector;101;-6919.389,-1524.655;Float;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;107;-6338.561,-1345.66;Float;False;Constant;_DivisionStrenght;Division Strenght;3;0;Create;0.49;0.45;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;108;-6627.821,-1464.218;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0.0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;111;-6055.857,-1455.55;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;1.0;False;2;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;113;-5813.001,-1495.409;Float;True;1;0;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;262;-5120.22,-1667.075;Float;False;1354.239;628.1451;Color Sombra;6;198;260;261;258;188;197;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;120;-5535.312,-1640.045;Float;True;Property;_Degradados;Degradados;4;0;Create;194f3ca1259a1d241a675089bdcc60b1;6fe2de297a0ff9a4b8f35b6cbaf534de;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;261;-5039.798,-1221.43;Float;False;Property;_Contraste_Sombra;Contraste_Sombra;7;0;Create;1;1.1;1;1.1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;251;-4280.84,-952.4942;Float;False;665.1149;464.5551;Modulo Luz;5;246;247;248;211;249;;1,1,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;198;-4997.468,-1486.234;Float;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;253;-5734.051,-3270.082;Float;False;1373.757;849.2211;Color;9;204;185;205;119;186;263;264;266;265;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;260;-4704.751,-1486.318;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;1.25;False;1;COLOR;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;247;-4225.63,-731.8392;Float;False;Tangent;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightAttenuation;246;-4230.84,-597.9367;Float;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;119;-5684.051,-3220.082;Float;True;Property;_Difuse;Difuse;0;0;Create;84508b93f15f2b64386ec07486afc7a3;92f6a5893ac768c41b25716b1575587e;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;186;-5666.25,-3014.657;Float;False;Property;_PowDiffuse;Pow Diffuse;2;0;Create;1;1;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;188;-4407.42,-1499.72;Float;False;Property;_ShadowColor;Shadow Color;3;0;Create;1,0.5588235,0.5588235,0;1,0.6862745,0.6862745,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LightColorNode;211;-4134.31,-902.494;Float;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.ClampOpNode;258;-4398.609,-1291.93;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0.5882353,0.5882353,0.5882353,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;248;-3951.326,-731.8382;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0.0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PowerNode;185;-5295.336,-3164.043;Float;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;205;-5323.977,-2931.241;Float;False;Property;_BrilloDifuse;Brillo Difuse;1;0;Create;1;1.25;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;266;-5015.546,-2586.807;Float;False;Property;_Sweat;Sweat;9;0;Create;0;22.7;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;264;-5447.664,-2762.198;Float;True;Property;_Sweat_test;Sweat_test;8;0;Create;667a235cfff6edf43b5bea26d2fb112b;667a235cfff6edf43b5bea26d2fb112b;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;249;-3784.723,-902.239;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;265;-4845.737,-2793.135;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;204;-4952.001,-3162.892;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;197;-4086.505,-1549.767;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;2;FLOAT;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;255;-3542.705,-1649.979;Float;False;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;263;-4604.744,-3038.119;Float;False;3;0;COLOR;0.0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;193;-2394.399,-2058.988;Float;False;539.3658;311.6136;Obscurece las sombras;2;189;192;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;256;-3476.363,-2689.102;Float;False;Property;_Poder_Sombra;Poder_Sombra;6;0;Create;0.5;0.3;0;0.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;123;-3394.784,-2352.509;Float;False;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;254;-3026.762,-2758.369;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;192;-2367.838,-1831.222;Float;False;Property;_PowerAll;Power All;5;0;Create;1;0.17;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;189;-2056.837,-1955.189;Float;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-1415.526,-2201.038;Float;False;True;2;Float;ASEMaterialInspector;0;0;CustomLighting;DDD/Body_Shader 1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;True;0.05;0.1470588,0.1470588,0.1470588,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0.0,0,0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;108;0;101;0
WireConnection;108;1;103;0
WireConnection;111;0;108;0
WireConnection;111;1;107;0
WireConnection;111;2;107;0
WireConnection;113;0;111;0
WireConnection;120;1;113;0
WireConnection;198;0;120;0
WireConnection;260;1;198;0
WireConnection;260;0;261;0
WireConnection;258;0;260;0
WireConnection;248;0;247;0
WireConnection;248;1;246;0
WireConnection;185;0;119;0
WireConnection;185;1;186;0
WireConnection;249;0;211;0
WireConnection;249;1;248;0
WireConnection;265;0;264;4
WireConnection;265;1;266;0
WireConnection;204;1;185;0
WireConnection;204;0;205;0
WireConnection;197;0;120;0
WireConnection;197;1;188;0
WireConnection;197;2;258;0
WireConnection;255;0;249;0
WireConnection;255;1;197;0
WireConnection;263;0;204;0
WireConnection;263;1;264;0
WireConnection;263;2;265;0
WireConnection;123;0;263;0
WireConnection;123;1;255;0
WireConnection;254;0;263;0
WireConnection;254;1;123;0
WireConnection;254;2;256;0
WireConnection;189;0;254;0
WireConnection;189;1;192;0
WireConnection;0;13;254;0
ASEEND*/
//CHKSM=CF06D8F42F96546060B3E88D4DB204927F74171C