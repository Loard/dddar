// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "DDD/Clothes_Shader"
{
	Properties
	{
		_ASEOutlineWidth( "Outline Width", Float ) = 0.05
		_ASEOutlineColor( "Outline Color", Color ) = (0.1470588,0.1470588,0.1470588,0)
		_Difuse("Difuse", 2D) = "white" {}
		_BrilloDifuse("Brillo Difuse", Range( 1 , 2)) = 1
		_PowDiffuse("Pow Diffuse", Range( 1 , 2)) = 1
		_Degradados("Degradados", 2D) = "white" {}
		_ShadowContrast("ShadowContrast", Range( 1 , 1.5)) = 1
		_BrilloDegradado("Brillo Degradado", Range( 1 , 1.5)) = 1
		_ShadowColor("Shadow Color", Color) = (1,0.5588235,0.5588235,0)
		_ShadowPower("Shadow Power", Range( 0.01 , 0.3)) = 0.01
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ }
		Cull Front
		CGPROGRAM
		#pragma target 3.0
		#pragma surface outlineSurf Outline nofog keepalpha noshadow noambient novertexlights nolightmap nodynlightmap nodirlightmap nometa noforwardadd vertex:outlineVertexDataFunc 
		uniform fixed4 _ASEOutlineColor;
		uniform fixed _ASEOutlineWidth;
		void outlineVertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			v.vertex.xyz += ( v.normal * _ASEOutlineWidth );
		}
		inline fixed4 LightingOutline( SurfaceOutput s, half3 lightDir, half atten ) { return fixed4 ( 0,0,0, s.Alpha); }
		void outlineSurf( Input i, inout SurfaceOutput o )
		{
			o.Emission = _ASEOutlineColor.rgb;
			o.Alpha = 1;
		}
		ENDCG
		

		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Off
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldNormal;
			INTERNAL_DATA
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			fixed3 Albedo;
			fixed3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			fixed Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float _BrilloDifuse;
		uniform sampler2D _Difuse;
		uniform float4 _Difuse_ST;
		uniform float _PowDiffuse;
		uniform float _BrilloDegradado;
		uniform float _ShadowContrast;
		uniform sampler2D _Degradados;
		uniform float4 _ShadowColor;
		uniform float _ShadowPower;


		float4 CalculateContrast( float contrastValue, float4 colorTarget )
		{
			float t = 0.5 * ( 1.0 - contrastValue );
			return mul( float4x4( contrastValue,0,0,t, 0,contrastValue,0,t, 0,0,contrastValue,t, 0,0,0,1 ), colorTarget );
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#if DIRECTIONAL
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			float2 uv_Difuse = i.uv_texcoord * _Difuse_ST.xy + _Difuse_ST.zw;
			float4 tex2DNode119 = tex2D( _Difuse, uv_Difuse );
			float4 temp_cast_0 = (_PowDiffuse).xxxx;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			float dotResult108 = dot( ase_worldNormal , ase_worldlightDir );
			float temp_output_113_0 = saturate( (dotResult108*0.49 + 0.49) );
			float2 temp_cast_1 = (temp_output_113_0).xx;
			float4 tex2DNode120 = tex2D( _Degradados, temp_cast_1 );
			float4 temp_output_242_0 = CalculateContrast(_ShadowContrast,tex2DNode120);
			UnityGI gi245 = gi;
			float3 diffNorm245 = ase_worldNormal;
			gi245 = UnityGI_Base( data, 1, diffNorm245 );
			float3 indirectDiffuse245 = gi245.indirect.diffuse + diffNorm245 * 0.0001;
			float4 temp_cast_3 = (_ShadowPower).xxxx;
			c.rgb = ( CalculateContrast(_BrilloDifuse,pow( tex2DNode119 , temp_cast_0 )) * CalculateContrast(_BrilloDegradado,pow( ( ( temp_output_242_0 * _ShadowColor ) * ( _LightColor0 * float4( ( indirectDiffuse245 + ase_lightAtten ) , 0.0 ) ) ) , temp_cast_3 )) ).rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14201
1927;29;1668;919;1890.873;1996.117;2.635424;True;False
Node;AmplifyShaderEditor.CommentaryNode;100;-5096.826,-1141.194;Float;False;558.214;366.9138;Comment;3;103;101;108;N . L;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;102;-4512.155,-1136.177;Float;False;723.599;290;Lambert Wrap or Half Lambert;3;113;111;107;Diffuse Wrap;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;103;-5029.264,-929.631;Float;False;1;0;FLOAT;0.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldNormalVector;101;-4988.389,-1089.631;Float;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DotProductOpNode;108;-4696.826,-1029.194;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0.0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;107;-4496.756,-923.6874;Float;False;Constant;_DivisionStrenght;Division Strenght;3;0;Create;0.49;0.49;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;111;-4214.054,-1033.577;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;1.0;False;2;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;113;-3971.202,-1073.436;Float;True;1;0;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;245;-2497.156,-781.8934;Float;False;Tangent;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightAttenuation;247;-2502.366,-647.9922;Float;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;241;-3479.424,-861.3685;Float;False;Property;_ShadowContrast;ShadowContrast;13;0;Create;1;1;1;1.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;120;-3667.41,-1109.314;Float;True;Property;_Degradados;Degradados;4;0;Create;2429be309ec037b4c99f6b10bbee81aa;194f3ca1259a1d241a675089bdcc60b1;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;188;-2827.725,-1174.107;Float;False;Property;_ShadowColor;Shadow Color;15;0;Create;1,0.5588235,0.5588235,0;0.9338235,0.9338235,0.9338235,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;246;-2157.854,-761.0928;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0.0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightColorNode;211;-2312.552,-957.902;Float;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleContrastOpNode;242;-3110.01,-1240.9;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;193;-1900.773,-853.301;Float;False;539.3658;311.6136;Obscurece las sombras;2;189;192;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;248;-2008.792,-840.3262;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;249;-2253.522,-1365.227;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;192;-1860.872,-619.9879;Float;False;Property;_ShadowPower;Shadow Power;16;0;Create;0.01;0.15;0.01;0.3;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;119;-2934.896,-1847.481;Float;True;Property;_Difuse;Difuse;0;0;Create;84508b93f15f2b64386ec07486afc7a3;d3c25cc41ddfcf24ea1b14700c83f62c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;213;-2038.564,-1177.206;Float;False;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;186;-2098.024,-1707.594;Float;False;Property;_PowDiffuse;Pow Diffuse;2;0;Create;1;1;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;194;-1192.076,-681.0415;Float;False;586.2532;183;Aclara los colores;2;190;191;;1,1,1,1;0;0
Node;AmplifyShaderEditor.PowerNode;185;-1710.913,-1865.075;Float;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;189;-1563.207,-749.5026;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;190;-1127.207,-599.4699;Float;False;Property;_BrilloDegradado;Brillo Degradado;14;0;Create;1;1;1;1.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;205;-1442.489,-1634.613;Float;False;Property;_BrilloDifuse;Brillo Difuse;1;0;Create;1;1.25;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;167;-3197.124,-2931.338;Float;False;1526.759;845.8296;Cambiamos el color y el patron;12;147;146;145;132;135;131;166;164;170;171;172;174;Color and Pattern Change;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;204;-1052.436,-1877.12;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;191;-791.8185,-631.0415;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;168;-1386.407,-2387.698;Float;True;Property;_Especial;Especial;5;1;[Toggle];Create;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;147;-2577.502,-2911.614;Float;False;Property;_Pattern01;Pattern 0/1;10;2;[Toggle];[Toggle];Create;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;146;-3142.99,-2695.447;Float;True;Global;SpecialPattern1;Special Pattern 1;12;0;Create;84508b93f15f2b64386ec07486afc7a3;84508b93f15f2b64386ec07486afc7a3;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;145;-3145.499,-2881.338;Float;True;Property;_SpecialPattern0;Special Pattern 0 ;11;0;Create;84508b93f15f2b64386ec07486afc7a3;84508b93f15f2b64386ec07486afc7a3;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ToggleSwitchNode;131;-2635.645,-2220.057;Float;False;Property;_Color01;Color 0/1;9;2;[Toggle];[Toggle];Create;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;172;-1889.105,-2473.318;Float;False;Property;_Basicoplus;Basico plus;7;1;[Toggle];Create;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;135;-3053.615,-2351.511;Float;False;Constant;_SpecialColor_1;Special Color_1;7;0;Create;0,0.8344827,1,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;166;-2359.512,-2766.819;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;218;-3686.667,-1404.644;Float;True;Property;_Degradados2;Degradados 2;3;0;Create;2429be309ec037b4c99f6b10bbee81aa;64b282229066c7444aff8dadc9fe8866;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;244;-3284.436,-1249.054;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;170;-2498.767,-2416.189;Float;False;Property;_ColorTextura;Color / Textura;8;1;[Toggle];Create;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;198;-2811.905,-914.2089;Float;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;171;-2116.881,-2319.636;Float;False;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;174;-2162.301,-2162.954;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;132;-3056.19,-2513.315;Float;False;Property;_SpecialColor_0;Special Color_0;6;0;Create;1,0,0,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;164;-2121.489,-2711.196;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;123;15.38579,-819.4236;Float;False;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;197;-2362.821,-1234.77;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;2;FLOAT;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;588.8524,-821.7889;Float;False;True;2;Float;ASEMaterialInspector;0;0;CustomLighting;DDD/Clothes_Shader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;True;0.05;0.1470588,0.1470588,0.1470588,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0.0,0,0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;108;0;101;0
WireConnection;108;1;103;0
WireConnection;111;0;108;0
WireConnection;111;1;107;0
WireConnection;111;2;107;0
WireConnection;113;0;111;0
WireConnection;120;1;113;0
WireConnection;246;0;245;0
WireConnection;246;1;247;0
WireConnection;242;1;120;0
WireConnection;242;0;241;0
WireConnection;248;0;211;0
WireConnection;248;1;246;0
WireConnection;249;0;242;0
WireConnection;249;1;188;0
WireConnection;213;0;249;0
WireConnection;213;1;248;0
WireConnection;185;0;119;0
WireConnection;185;1;186;0
WireConnection;189;0;213;0
WireConnection;189;1;192;0
WireConnection;204;1;185;0
WireConnection;204;0;205;0
WireConnection;191;1;189;0
WireConnection;191;0;190;0
WireConnection;168;0;119;0
WireConnection;168;1;172;0
WireConnection;147;0;145;0
WireConnection;147;1;146;0
WireConnection;131;0;132;0
WireConnection;131;1;135;0
WireConnection;172;0;164;0
WireConnection;172;1;171;0
WireConnection;166;0;145;0
WireConnection;166;1;132;0
WireConnection;218;1;113;0
WireConnection;244;0;218;0
WireConnection;244;1;120;0
WireConnection;170;0;132;0
WireConnection;170;1;145;0
WireConnection;198;0;242;0
WireConnection;171;0;170;0
WireConnection;171;1;174;0
WireConnection;174;0;119;0
WireConnection;164;0;166;0
WireConnection;164;1;119;0
WireConnection;123;0;204;0
WireConnection;123;1;191;0
WireConnection;197;0;242;0
WireConnection;197;1;188;0
WireConnection;197;2;198;0
WireConnection;0;13;123;0
ASEEND*/
//CHKSM=58ADAA4F6EC47199BF11CD387D3233B820F2973F