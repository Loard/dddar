// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "DDD/Face_Shader"
{
	Properties
	{
		_ASEOutlineWidth( "Outline Width", Float ) = 0.05
		_ASEOutlineColor( "Outline Color", Color ) = (0.1470588,0.1470588,0.1470588,0)
		_Face_Diff("Face_Diff", 2D) = "white" {}
		_BrilloDifuse("Brillo Difuse", Range( 1 , 2)) = 1
		_PowDiffuse("Pow Diffuse", Range( 1 , 2)) = 1
		_Shadow_Color("Shadow_Color", Color) = (1,0.5588235,0.5588235,0)
		_Degradado("Degradado", 2D) = "white" {}
		_Poder_Sombra("Poder_Sombra", Range( 0 , 0.5)) = 0.5
		_Contraste_sombra("Contraste_sombra", Range( 1 , 1.1)) = 1
		_Face_Mask("Face_Mask", 2D) = "white" {}
		_Rubor_Pow("Rubor_Pow", Range( 0 , 2)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ }
		Cull Front
		CGPROGRAM
		#pragma target 3.0
		#pragma surface outlineSurf Outline nofog keepalpha noshadow noambient novertexlights nolightmap nodynlightmap nodirlightmap nometa noforwardadd vertex:outlineVertexDataFunc 
		uniform fixed4 _ASEOutlineColor;
		uniform fixed _ASEOutlineWidth;
		void outlineVertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			v.vertex.xyz += ( v.normal * _ASEOutlineWidth );
		}
		inline fixed4 LightingOutline( SurfaceOutput s, half3 lightDir, half atten ) { return fixed4 ( 0,0,0, s.Alpha); }
		void outlineSurf( Input i, inout SurfaceOutput o )
		{
			o.Emission = _ASEOutlineColor.rgb;
			o.Alpha = 1;
		}
		ENDCG
		

		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldNormal;
			INTERNAL_DATA
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			fixed3 Albedo;
			fixed3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			fixed Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float _BrilloDifuse;
		uniform sampler2D _Face_Diff;
		uniform float4 _Face_Diff_ST;
		uniform sampler2D _Face_Mask;
		uniform float4 _Face_Mask_ST;
		uniform float _Rubor_Pow;
		uniform float _PowDiffuse;
		uniform sampler2D _Degradado;
		uniform float4 _Shadow_Color;
		uniform float _Contraste_sombra;
		uniform float _Poder_Sombra;


		float4 CalculateContrast( float contrastValue, float4 colorTarget )
		{
			float t = 0.5 * ( 1.0 - contrastValue );
			return mul( float4x4( contrastValue,0,0,t, 0,contrastValue,0,t, 0,0,contrastValue,t, 0,0,0,1 ), colorTarget );
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#if DIRECTIONAL
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			float2 uv_Face_Diff = i.uv_texcoord * _Face_Diff_ST.xy + _Face_Diff_ST.zw;
			float2 uv_Face_Mask = i.uv_texcoord * _Face_Mask_ST.xy + _Face_Mask_ST.zw;
			float4 tex2DNode252 = tex2D( _Face_Mask, uv_Face_Mask );
			float4 lerpResult255 = lerp( tex2D( _Face_Diff, uv_Face_Diff ) , tex2DNode252 , ( tex2DNode252.a * _Rubor_Pow ));
			float4 temp_cast_0 = (_PowDiffuse).xxxx;
			float4 temp_output_204_0 = CalculateContrast(_BrilloDifuse,pow( lerpResult255 , temp_cast_0 ));
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			float dotResult265 = dot( ase_worldNormal , ase_worldlightDir );
			float2 temp_cast_1 = (saturate( (dotResult265*0.49 + 0.49) )).xx;
			float4 tex2DNode269 = tex2D( _Degradado, temp_cast_1 );
			float4 clampResult280 = clamp( CalculateContrast(_Contraste_sombra,( 1.0 - tex2DNode269 )) , float4( 0,0,0,0 ) , float4( 0.5882353,0.5882353,0.5882353,0 ) );
			float4 lerpResult281 = lerp( tex2DNode269 , _Shadow_Color , clampResult280.r);
			UnityGI gi283 = gi;
			float3 diffNorm283 = ase_worldNormal;
			gi283 = UnityGI_Base( data, 1, diffNorm283 );
			float3 indirectDiffuse283 = gi283.indirect.diffuse + diffNorm283 * 0.0001;
			float4 lerpResult291 = lerp( temp_output_204_0 , ( temp_output_204_0 * ( lerpResult281 * ( _LightColor0 * float4( ( indirectDiffuse283 + ase_lightAtten ) , 0.0 ) ) ) ) , _Poder_Sombra);
			c.rgb = lerpResult291.rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14201
2040;124;1668;919;3303.125;2274.313;2.525424;True;False
Node;AmplifyShaderEditor.CommentaryNode;259;-5294.061,-1374.858;Float;False;1854.811;555.8474;Sombras;3;269;261;260;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;260;-5252.765,-1229.15;Float;False;558.214;366.9138;Comment;3;265;263;262;N . L;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;261;-4578.892,-1211.082;Float;False;723.599;290;Lambert Wrap or Half Lambert;3;267;266;264;Diffuse Wrap;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;263;-5185.201,-1017.587;Float;False;1;0;FLOAT;0.0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldNormalVector;262;-5144.327,-1177.587;Float;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;264;-4563.493,-998.5918;Float;False;Constant;_Float0;Float 0;3;0;Create;0.49;0.45;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;265;-4852.753,-1117.15;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0.0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;266;-4280.789,-1108.482;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;1.0;False;2;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;267;-4037.93,-1148.341;Float;True;1;0;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;268;-3122.288,-1334.865;Float;False;1354.239;628.1451;Color Sombra;6;281;280;279;274;271;270;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;269;-3759.236,-1292.977;Float;True;Property;_Degradado;Degradado;4;0;Create;194f3ca1259a1d241a675089bdcc60b1;c9f73148a4ef1cb4a971b2c45075223a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;252;-2831.886,-2169.2;Float;True;Property;_Face_Mask;Face_Mask;8;0;Create;7da5f00c9b890714bb81029f9d8be346;7da5f00c9b890714bb81029f9d8be346;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;257;-2941.093,-1905.566;Float;False;Property;_Rubor_Pow;Rubor_Pow;9;0;Create;0;0;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;271;-3072.288,-965.2671;Float;False;Property;_Contraste_sombra;Contraste_sombra;7;0;Create;1;0;1;1.1;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;270;-2984.325,-1219.011;Float;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;277;-2549.769,-543.0378;Float;False;665.1149;464.5551;Modulo Luz;5;289;288;287;284;283;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;256;-2609.429,-1896.24;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;251;-2820.338,-2363.373;Float;True;Property;_Face_Diff;Face_Diff;0;0;Create;4ea1337b6195fb0428404b117e19502c;4ea1337b6195fb0428404b117e19502c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LightAttenuation;284;-2499.769,-188.4836;Float;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;274;-2698.523,-1216.329;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;1.25;False;1;COLOR;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;283;-2494.559,-322.384;Float;False;Tangent;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ClampOpNode;280;-2400.68,-959.7191;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0.5882353,0.5882353,0.5882353,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;279;-2413.343,-1167.51;Float;False;Property;_Shadow_Color;Shadow_Color;3;0;Create;1,0.5588235,0.5588235,0;1,0.6862069,0.6862069,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;186;-2404.64,-1936.494;Float;False;Property;_PowDiffuse;Pow Diffuse;2;0;Create;1;1;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;255;-2383.076,-2225.362;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;2;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightColorNode;288;-2403.24,-493.0378;Float;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleAddOpNode;287;-2220.257,-322.3831;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0.0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PowerNode;185;-2050.909,-2217.503;Float;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;281;-2033.053,-1284.865;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;2;FLOAT;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;205;-2062.362,-1970.934;Float;False;Property;_BrilloDifuse;Brillo Difuse;1;0;Create;1;1.5;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;289;-2053.654,-492.7828;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;292;-1626.737,-1134.394;Float;False;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;204;-1774.026,-2213.09;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;290;-103.5912,-1299.15;Float;False;539.3658;311.6136;Obscurece las sombras;2;293;294;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;285;-1574.735,-2108.746;Float;False;Property;_Poder_Sombra;Poder_Sombra;6;0;Create;0.5;1;0;0.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;286;-1308.387,-1733.654;Float;False;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;294;93.99985,-1193.738;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;291;-1000.875,-2185.477;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;293;-77.03014,-1071.385;Float;False;Property;_Power_All;Power_All;5;0;Create;1;0.17;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;588.8524,-821.7889;Float;False;True;2;Float;ASEMaterialInspector;0;0;CustomLighting;DDD/Face_Shader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;True;0.05;0.1470588,0.1470588,0.1470588,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0.0,0,0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;265;0;262;0
WireConnection;265;1;263;0
WireConnection;266;0;265;0
WireConnection;266;1;264;0
WireConnection;266;2;264;0
WireConnection;267;0;266;0
WireConnection;269;1;267;0
WireConnection;270;0;269;0
WireConnection;256;0;252;4
WireConnection;256;1;257;0
WireConnection;274;1;270;0
WireConnection;274;0;271;0
WireConnection;280;0;274;0
WireConnection;255;0;251;0
WireConnection;255;1;252;0
WireConnection;255;2;256;0
WireConnection;287;0;283;0
WireConnection;287;1;284;0
WireConnection;185;0;255;0
WireConnection;185;1;186;0
WireConnection;281;0;269;0
WireConnection;281;1;279;0
WireConnection;281;2;280;0
WireConnection;289;0;288;0
WireConnection;289;1;287;0
WireConnection;292;0;281;0
WireConnection;292;1;289;0
WireConnection;204;1;185;0
WireConnection;204;0;205;0
WireConnection;286;0;204;0
WireConnection;286;1;292;0
WireConnection;294;0;291;0
WireConnection;294;1;293;0
WireConnection;291;0;204;0
WireConnection;291;1;286;0
WireConnection;291;2;285;0
WireConnection;0;13;291;0
ASEEND*/
//CHKSM=9CBBAC40920BCC118E10A1EFE0C8EDDBEFDD7C1C